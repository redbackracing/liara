#!/usr/bin/python3

import argparse
import json
import time
import datetime
import math
import curses
import signal
import sys

from curio import run, spawn, sleep, Queue
from curio.file import aopen
from curio.socket import socket, AF_INET, SOCK_DGRAM

data_queue = Queue()

# Event log for the user interface.
last_event = (time.time(), 'Initialised server')

# Flag for enabling connections from Benezia
should_send = True

# Flag to enable latency logging
latency = False

# Default latency log filename
latency_logfile = 'latency.log'


async def benezia_link(address, port):
    sock = socket(AF_INET, SOCK_DGRAM)
    sock.bind((address, port))

    while True:
        # Receive connections on the socket from the Benezia server.
        message, address = await sock.recvfrom(200)

        # Decode as a string and load request of JSON.
        message = message.decode('UTF-8')
        benezia_packet = json.loads(message)

        update_last_event(
            'Received request from Benezia at {0}:\n{1}'.format(address, benezia_packet))

        # Only send data if the toggle is set, otherwise send a blank response.
        if should_send:
            if benezia_packet['request'] == '1':
                # Benezia has requested data, so return the oldest non-stale blob.
                time_received, current_blob = await data_queue.get()

                while (is_stale(time_received, time_limit=5)):
                    time_received, current_blob = await data_queue.get()

                # Log latency if required.
                if latency:
                    await log_latency(latency_logfile, time_received, time.time())

                await sock.sendto(current_blob.encode('utf_8'), address)

                update_last_event(
                    'Returned data to Benezia at {0}:\n{1}'.format(address, current_blob))

        else:
            await sock.sendto('{}'.encode('utf-8'), address)


async def datalogger_link(address, port):

    sock = socket(AF_INET, SOCK_DGRAM)
    sock.bind((address, port))

    while True:
        # Receive connections on the socket from the datalogger.
        message, address = await sock.recvfrom(1000)

        # update current_blob as the message recieved
        current_blob = message.decode('utf_8')

        update_last_event('Received data from datalogger at {0}: {1}'.format(address, current_blob))

        await data_queue.put((time.time(), current_blob))


async def main(args):
    global latency_logfile

    print(
        'Liara started on ports {0} (datalogger) and {1} (Benezia)'.format(args.dport, args.bport))

    latency_logfile = args.logfile

    if args.l:
        await toggle_latency_logging()

    interface_task = await spawn(interface)
    datalogger_server_task = await spawn(datalogger_link, '0.0.0.0', args.dport)
    benezia_server_task = await spawn(benezia_link, '0.0.0.0', args.bport)

    # Wait for interface to finish to close down server.
    await interface_task.join()

    # Cancel async tasks.
    datalogger_server_task.cancel()
    benezia_server_task.cancel()

    # Reset the curses display.
    reset_display()

    sys.exit(0)


def is_stale(time_received, time_limit=5):
    '''Determine if the blob is stale, i.e. it was received over n seconds
    ago.'''
    return time.time() - time_received > time_limit


async def log_latency(filename, last_received_time, sent_time):
    '''Calculate the latency from the time data was received and sent,
    writing to a logfile.'''

    latency = sent_time - last_received_time
    latency_out = '{0}ms\n'.format(latency)

    async with aopen(filename, 'a') as f:
        await f.write(latency_out)


async def toggle_latency_logging():
    '''Turn latency logging on or off.'''
    global latency

    if latency:
        latency = False
    else:
        # Initialise logfile with current time as heading.
        async with aopen(latency_logfile, 'a') as f:
            now = datetime.datetime.now()
            await f.write(now.strftime('%d-%b-%Y %H:%M') + '\n')

        latency = True


def update_last_event(message):
    '''Given a message, make it the last logged event along with a timestamp.'''
    global last_event

    last_event = (time.time(), message)


async def interface():
    '''Update the curses interface.'''
    global should_send

    screen = initialise_screen()

    status_window, control_window, update_window = initialise_windows()

    while True:
        screen.clear()

        # Get a character of input.
        c = screen.getch()

        if c == ord('s'):
            # Toggle data sending when s is pressed.
            should_send = not should_send
        elif c == ord('l'):
            # Toggle latency logging when l is pressed.
            await toggle_latency_logging()
        elif c == ord('q'):
            # Quit the program when q is pressed.
            return

        update_status(status_window, should_send, latency)
        update_updates(update_window, last_event)
        draw_controls(control_window)

        await sleep(0.1)


def initialise_screen():
    '''Set up the curses display.'''
    screen = curses.initscr()
    screen.nodelay(True)

    curses.noecho()
    curses.cbreak()

    return screen


def initialise_windows():
    '''Set up the windows on the curses display.'''
    screen_height = curses.LINES
    screen_width = curses.COLS

    # Window for live information about the server.
    status_window = curses.newwin(screen_height - 6, math.floor(screen_width / 2), 0, 0)

    control_window = curses.newwin(6, math.floor(screen_width / 2), screen_height - 6, 0)

    update_window = curses.newwin(screen_height,
                                  math.ceil(screen_width / 2), 0, math.floor(screen_width / 2) + 1)

    return status_window, control_window, update_window


def update_status(window, should_send, latency, online=True):
    '''Output status information to the screen.'''
    window.clear()

    window.addstr(1, 1, 'Status', curses.A_UNDERLINE)

    if online:
        window.addstr(2, 1, 'State: online')
    else:
        window.addstr(2, 1, 'State: offline')

    if should_send:
        window.addstr(3, 1, 'Client connections: allowed')
    else:
        window.addstr(3, 1, 'Client connections: ignored')

    if latency:
        window.addstr(4, 1, 'Latency logging: enabled')
    else:
        window.addstr(4, 1, 'Latency logging: disabled')

    window.border()
    window.refresh()


def update_updates(window, last_event):
    '''Output the last event logged to the screen.'''

    window.addstr(1, 1, 'Live actions', curses.A_UNDERLINE)

    window.addstr(2, 1, time.strftime('%H:%M:%S', time.localtime(last_event[0])))
    window.addstr(3, 1, last_event[1])

    window.border()
    window.refresh()


def draw_controls(window):
    '''Draw a list of controls onto the screen.'''
    window.addstr(1, 1, 'Controls', curses.A_UNDERLINE)

    window.addstr(2, 1, 'Press s to toggle allowing client connections')
    window.addstr(3, 1, 'Press l to toggle latency logging')
    window.addstr(4, 1, 'Press q to quit')

    window.border()

    window.refresh()


def reset_display():
    '''Clear the curses display.'''
    curses.nocbreak()
    curses.echo()
    curses.endwin()


if __name__ == '__main__':
    # Parse arguments from command line.
    parser = argparse.ArgumentParser()

    # The port to run Liara on
    parser.add_argument(
        'dport',
        type=int,
        default=12000,
        help='Port to run Liara on for the datalogger connection',
        nargs='?')
    parser.add_argument(
        'bport',
        type=int,
        default=12001,
        help='Port to run Liara on for the Benezia connection',
        nargs='?')

    # The number of seconds to keep the same blob before dropping
    parser.add_argument(
        'blob_hold_time',
        type=int,
        default=60,
        nargs='?',
        help='Number of seconds to keep same blob before dropping')

    # Enable latency calculation
    parser.add_argument(
        '-l',
        action='store_true',
        help='Flag to turn on latency calculation for C++ application to web server')

    # Specify latency log filename
    parser.add_argument(
        'logfile', type=str, default='latency.log', nargs='?', help='Name of the latency log file')

    args = parser.parse_args()

    run(main, args)
